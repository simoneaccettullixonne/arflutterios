import UIKit
import Foundation
import ARKit
import Combine
import GLTFSceneKit

class ArModelBuilder: NSObject {

    // Creates a node from a given gltf2 (.gltf) model in the Flutter assets folder
//    func makeNodeFromGltf(name: String, modelPath: String, transformation: Array<NSNumber>?) -> SCNNode? {
//
//        var scene: SCNScene
//        let node: SCNNode = SCNNode()
//
//        do {
//            let sceneSource = try GLTFSceneSource(named: modelPath)
//            scene = try sceneSource.scene()
//
//            for child in scene.rootNode.childNodes {
//                child.scale = SCNVector3(0.01,0.01,0.01) // Compensate for the different model dimension definitions in iOS and Android (meters vs. millimeters)
//                //child.eulerAngles.z = -.pi // Compensate for the different model coordinate definitions in iOS and Android
//                //child.eulerAngles.y = -.pi // Compensate for the different model coordinate definitions in iOS and Android
//                node.addChildNode(child.flattenedClone())
//            }
//
//            node.name = name
//            if let transform = transformation {
//                node.transform = deserializeMatrix4(transform)
//            }
//
//            return node
//        } catch {
//            print("\(error.localizedDescription)")
//            return nil
//        }
//    }
    
    // Creates a node form a given glb model path
    func makeNodeFromWebGlb(name: String, modelURL: String,
                            transformation: Array<NSNumber>?,
                            onComplete:@escaping (SCNNode) -> (),
                            onFailure:@escaping (Error?) -> ()) {
        
        let handler: (URL?, URLResponse?, Error?) -> Void = {(url: URL?, urlResponse: URLResponse?, error: Error?) -> Void in
            // If response code is not 200, link was invalid, so return
            if ((urlResponse as? HTTPURLResponse)?.statusCode != 200) {
                print("makeNodeFromWebGltf received non-200 response code")
                onFailure(error)
            } else {
                guard let fileURL = url else { return }
                do {
                    let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
                    let documentsDirectory = paths[0]
                    let targetURL = documentsDirectory.appendingPathComponent(urlResponse!.url!.lastPathComponent)
                    
                    try? FileManager.default.removeItem(at: targetURL) //remove item if it's already there
                    try FileManager.default.copyItem(at: fileURL, to: targetURL)

                    do {
                        var node = SCNNode()
                        let sceneSource = GLTFSceneSource(url: targetURL)
                        let scene = try sceneSource.scene()

                        scene.rootNode.enumerateHierarchy { (node, stop) in
                                node.childNodes.forEach({
                                    if ($0.childNodes.isEmpty) {
                                        $0.name = name
                                    }
                                })
                              }
                        
                        if let n = scene.rootNode.childNodes.first {
                            node.addChildNode(n)
                        }
                        
//                        node.scale = SCNVector3(0.1,0.1,0.1)
                        node.name = name
                        onComplete(node)

                    } catch {
                        onFailure(error)
                    }
                    
                    // Delete file to avoid cluttering device storage (at some point, caching can be included)
                    try FileManager.default.removeItem(at: targetURL)
                } catch {
                    onFailure(error)
                }
            }
        }

        let downloadTask = URLSession.shared.downloadTask(with: URL(string: modelURL)!, completionHandler: handler)
        downloadTask.resume()
    }
}
