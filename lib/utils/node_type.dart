/// Determines which types of nodes the plugin supports
enum NodeType {
  local, // Node with renderable with fileending .gltf in the Flutter asset folder
  web, // Node with renderable with fileending .glb loaded from the internet during runtime
}